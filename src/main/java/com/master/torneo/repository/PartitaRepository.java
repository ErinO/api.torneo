package com.master.torneo.repository;

import com.master.torneo.model.Partita;
import org.springframework.data.repository.CrudRepository;

public interface PartitaRepository extends CrudRepository<Partita, Long> {
}
