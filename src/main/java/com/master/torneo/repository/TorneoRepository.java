package com.master.torneo.repository;

import com.master.torneo.model.Torneo;
import org.springframework.data.repository.CrudRepository;

public interface TorneoRepository extends CrudRepository<Torneo, Long> {
}
