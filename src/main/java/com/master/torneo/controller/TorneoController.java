package com.master.torneo.controller;

import com.google.common.collect.Sets;
import com.master.torneo.message.Response;
import com.master.torneo.model.Partita;
import com.master.torneo.model.Squadra;
import com.master.torneo.model.Torneo;
import com.master.torneo.service.api.PartitaService;
import com.master.torneo.service.api.TorneoService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.google.common.collect.Sets.cartesianProduct;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin(value = "*")
public class TorneoController {

    @Autowired
    TorneoService torneoService;

    @Autowired
    PartitaService partitaService;

    @RequestMapping(value = "/tournaments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response getListaTornei() {
        List<Torneo> tornei = torneoService.getTornei();
        return new Response("Done", tornei);
    }

    @RequestMapping(value = "/tournaments/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response getSingoloTorneo(@PathVariable("id") Long id) {
        Torneo t = torneoService.getTorneoById(id);
        return new Response("Done", t);
    }

    @RequestMapping(value = "/newtournament", method = POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response creaNuovoTorneo(@RequestBody String jsonString) {
        JSONObject jsob = new JSONObject(jsonString);
        JSONArray squadre = jsob.getJSONArray("squadre");
        HashSet<Squadra> squalist = new HashSet<>();
        for (int i = 0; i < squadre.length(); i++) {
            Squadra sq = new Squadra(squadre.getString(i));
            squalist.add(sq);
        }
        Torneo t = new Torneo(jsob.getString("nome"), jsob.getString("tipo"), squalist, null);
        Torneo salvato = torneoService.save(t);
        generaPartite(salvato.getSquadre(), salvato);

        return new Response("Done", salvato.getId());
    }

    private void generaPartite(Set<Squadra> squadre, Torneo torneo) {

        String tipologia = torneo.getTipo();
        Iterator<Squadra> iter = squadre.iterator();
        if (tipologia.equals("eliminazione")) {
            for (int i = 0; i < squadre.size(); i += 2) {
                long sq1 = iter.next().getId();
                long sq2 = iter.next().getId();
                Partita p = new Partita(torneo.getId(), sq1, sq2, 1);
                partitaService.save(p);
            }
        } else if (tipologia.equals("campionato")) {

            Set<Set<Squadra>> combi = Sets.combinations(squadre, 2);
            ArrayList<Set<Squadra>> combiArray = new ArrayList<>(combi);

            for (int i = 0; i < squadre.size() - 1; i++) {

                Iterator<Set<Squadra>> iterCoppie = combiArray.iterator();

                Set<Squadra> primaCoppia = iterCoppie.next();
                Iterator<Squadra> iterParta = primaCoppia.iterator();
                ArrayList<Long> uscite = new ArrayList<>();
                uscite.add(iterParta.next().getId());
                uscite.add(iterParta.next().getId());

                ArrayList<Set<Squadra>> partiteDelTurno = new ArrayList<>();
                partiteDelTurno.add(primaCoppia);
                iterCoppie.remove();

                while (partiteDelTurno.size() < squadre.size() / 2) {
                    Set<Squadra> setIncon = iterCoppie.next();
                    Object[] incontro = setIncon.toArray();
                    long idSquadra1 = ((Squadra) incontro[0]).getId();
                    long idSquadra2 = ((Squadra) incontro[1]).getId();
                    if (!uscite.contains(idSquadra1) && !uscite.contains(idSquadra2)) {
                        uscite.add(idSquadra1);
                        uscite.add(idSquadra2);
                        partiteDelTurno.add(setIncon);
                        iterCoppie.remove();
                    }
                }

                for (Set<Squadra> s : partiteDelTurno) {
                    Iterator<Squadra> iterTurno = s.iterator();
                    Partita p = new Partita(
                            torneo.getId(),
                            iterTurno.next().getId(),
                            iterTurno.next().getId(),
                            i+1);
                    partitaService.save(p);
                }
            }
        }
    }
}
