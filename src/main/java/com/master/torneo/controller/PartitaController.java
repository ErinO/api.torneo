package com.master.torneo.controller;

import com.master.torneo.message.Response;
import com.master.torneo.model.Partita;
import com.master.torneo.model.Squadra;
import com.master.torneo.model.Torneo;
import com.master.torneo.service.api.PartitaService;
import com.master.torneo.service.api.TorneoService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin(value = "*")
public class PartitaController {

    @Autowired
    TorneoService torneoService;

    @Autowired
    PartitaService partitaService;

    @RequestMapping(value = "/updateResults", method = POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Response inserisciRisultati(@RequestBody String jsonString) {

        JSONArray risultati = (JSONArray) new JSONObject(jsonString).get("data");

        ArrayList<Partita> partiteAggiornate = new ArrayList<>();
        for (Object o : risultati) {
            JSONObject pa = (JSONObject) o;
            partiteAggiornate.add(new Partita(
                    pa.getInt("id"),
                    pa.getInt("id_torneo"),
                    pa.getInt("squadra1"),
                    pa.getInt("squadra2"),
                    pa.getInt("goal1"),
                    pa.getInt("goal2"),
                    pa.getInt("turno")
            ));
        }
        for (Partita p : partiteAggiornate) {
            System.out.println(p.getTorneo());
        }

        boolean turnoFinito = partitaService.updateTurno(partiteAggiornate);

        Torneo torneo = torneoService.getTorneoById(partiteAggiornate.get(0).getTorneo());
        if (torneo.getTipo().equals("eliminazione") && turnoFinito) {
            int turno = partiteAggiornate.get(0).getTurno();
            partitaService.addTurno(torneo.getId(), turno);
        }
        return new Response("Done", true);
    }
}
