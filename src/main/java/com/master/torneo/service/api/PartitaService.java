package com.master.torneo.service.api;

import com.master.torneo.model.Partita;

import java.util.List;

public interface PartitaService {

    Partita getPartita(long id);

    Partita save(Partita p);

    boolean updateTurno(List<Partita> list);

    boolean addTurno(long id, int turno);
}
