package com.master.torneo.service.api;

import com.master.torneo.model.Torneo;

import java.util.List;

public interface TorneoService {

    List<Torneo> getTornei();

    Torneo getTorneoById(Long id);

    Torneo save(Torneo torneo);

}
