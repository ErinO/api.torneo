package com.master.torneo.service.impl;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.master.torneo.model.Partita;
import com.master.torneo.repository.PartitaRepository;
import com.master.torneo.service.api.PartitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("partitaService")
public class PartitaServiceImpl implements PartitaService {

    @Autowired
    PartitaRepository partitaRepository;

    @Override
    public Partita save(Partita pa) {
        return partitaRepository.save(pa);
    }

    @Override
    public boolean updateTurno(List<Partita> list) {

        if (list.isEmpty()) {
            return false;
        }

        long torneoId = list.get(0).getTorneo();
        int turnoId = list.get(0).getTurno();

        partitaRepository.save(list);
        Iterable<Partita> allPartite = partitaRepository.findAll();


        Predicate<Partita> pSelectPartite = input -> (input.getTurno() == turnoId) &&
                (input.getTorneo() == torneoId) &&
                (!input.isPlayed());

        Iterable<Partita> partiteTorneo = Iterables.filter(allPartite, pSelectPartite);

        /**
         * if 0 partite not played means that the torneo is finished so return TRUE to create new torneo
         * else there are still partite to play so do not create new torneo return FALSE
         */
        return Iterables.size(partiteTorneo) == 0;
    }

    @Override
    public boolean addTurno(long id_torneo, int turno) {

        try {
            Iterable<Partita> allPartite = partitaRepository.findAll();

            Predicate<Partita> pSelectPartite = input -> (input.getTorneo() == id_torneo) &&
                    (input.getTurno() == turno) &&
                    (input.isPlayed());

            Iterable<Partita> partiteTorneo = Iterables.filter(allPartite, pSelectPartite);

            List<Long> squadreVincitrici = new ArrayList<>();
            for (Partita p : partiteTorneo) {
                if (p.getGoal1() > p.getGoal2())
                    squadreVincitrici.add(p.getSquadra1());
                else
                    squadreVincitrici.add(p.getSquadra2());
            }
            Collections.shuffle(squadreVincitrici);

            List<Partita> newTurno = new ArrayList<>();
            for (int i = 0; i < squadreVincitrici.size(); i = i + 2) {
                newTurno.add(new Partita(
                        id_torneo,
                        squadreVincitrici.get(i),
                        squadreVincitrici.get(i + 1),
                        turno + 1));
            }
            partitaRepository.save(newTurno);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Partita getPartita(long id) {
        return null;
    }

}
