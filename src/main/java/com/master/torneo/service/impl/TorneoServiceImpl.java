package com.master.torneo.service.impl;

import com.master.torneo.model.Torneo;
import com.master.torneo.repository.TorneoRepository;
import com.master.torneo.service.api.TorneoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;

import java.util.List;

@Service("torneoService")
public class TorneoServiceImpl implements TorneoService {

    @Autowired
    private TorneoRepository torneoRepository;

    public List<Torneo> getTornei() {
        return Lists.newArrayList(torneoRepository.findAll());
    }

    public Torneo getTorneoById(Long id) {
        return torneoRepository.findOne(id);
    }

    @Override
    public Torneo save(Torneo torneo) {
        return torneoRepository.save(torneo);
    }
}
