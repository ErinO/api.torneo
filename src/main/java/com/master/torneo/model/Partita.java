package com.master.torneo.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "partita")
public class Partita implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "id_torneo")
    private long torneo;

    @Column(name = "squadra1")
    private long squadra1;

    @Column(name = "squadra2")
    private long squadra2;

    @Column(name = "goal1")
    private int goal1;

    @Column(name = "goal2")
    private int goal2;

    @Column(name = "turno")
    private int turno;

    public Partita() {}

    public Partita(long torneoid, long squadra1, long squadra2, int turno) {
        this.torneo = torneoid;
        this.squadra1 = squadra1;
        this.squadra2 = squadra2;
        this.goal1 = -1;
        this.goal2 = -1;
        this.turno = turno;
    }

    public Partita(long id, long torneoid, long squadra1, long squadra2, int goal1, int goal2, int turno) {
        this.id = id;
        this.torneo = torneoid;
        this.squadra1 = squadra1;
        this.squadra2 = squadra2;
        this.goal1 = goal1;
        this.goal2 = goal2;
        this.turno = turno;
    }

    public long getTorneo() { return torneo; }

    public long getId() {
        return id;
    }

    public long getSquadra1() {
        return squadra1;
    }

    public long getSquadra2() {
        return squadra2;
    }

    public int getGoal1() { return goal1; }

    public int getGoal2() {
        return goal2;
    }

    public int getTurno() {
        return turno;
    }

    public boolean isPlayed() { return goal1 != -1 && goal2 != -1;  }

}
