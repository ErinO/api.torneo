package com.master.torneo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="torneo")
public class Torneo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "tipo")
    private String tipo;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "iscrizione",
            joinColumns = { @JoinColumn(name = "id_torneo") },
            inverseJoinColumns = { @JoinColumn(name = "id_squadra")}
    )
    Set<Squadra> squadre = new HashSet<>();

    @OneToMany(mappedBy = "torneo")
    Set<Partita> partite = new HashSet<>();

    public Torneo() {}

    public Torneo(String nome, String tipo, Set<Squadra> squadre, Set<Partita> parte) {
        this.nome = nome;
        this.tipo = tipo;
        this.squadre = squadre;
        this.partite = parte;
    }

    public long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getTipo() { return tipo; }

    public Set<Squadra> getSquadre() {
        return squadre;
    }

    public Set<Partita> getPartite() {
        return partite;
    }

}
